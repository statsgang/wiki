# wiki

This repository is just to house the general [StatsGang](https://gitlab.com/statsgang/) [wiki](https://gitlab.com/statsgang/wiki/-/wikis/home) which includes details about the structure of the group, upcoming plans and serves as a global wiki for all the topics covered (rather than having them be contained to individual projects).
